using Godot;

namespace MonoPixelPlatformer.Scripts.Characters.Player
{
    public class PlayerController: KinematicBody2D
    {
        [Export] private float m_acceleration = 512;
        [Export] private float m_maxSpeed = 64;
        [Export] private float m_friction = 0.25f;
        [Export] private float m_airResistance = 0.02f;
        [Export] private float m_gravity = 200;
        [Export] private float m_jumpForce = 128;
        
        private Vector2 m_motion = Vector2.Zero;
        private AnimatedSprite m_animatedSprite;

        public override void _Ready()
        {
            m_animatedSprite = GetNode<AnimatedSprite>("AnimatedSprite");
        }

        public override void _PhysicsProcess(float delta)
        {
            float xInput = Input.GetActionStrength("ui_right") - Input.GetActionStrength("ui_left");

            if (xInput != 0)
            {
                m_animatedSprite.Play("Walk");
                m_motion.x += xInput * m_acceleration * delta;
                m_motion.x = Mathf.Clamp(m_motion.x, -m_maxSpeed, m_maxSpeed);
                m_animatedSprite.FlipH = xInput < 0;
            }
            else
            {
                m_animatedSprite.Play("Idle");
            }

            m_motion.y += m_gravity * delta;

            if (IsOnFloor())
            {
                if (xInput == 0)
                {
                    m_motion.x = Mathf.Lerp(m_motion.x, 0, m_friction);
                }
                if (Input.IsActionJustPressed("ui_select"))
                {
                    m_motion.y = -m_jumpForce;
                }
            }

            else
            {
                if (Input.IsActionJustReleased("ui_select") && m_motion.y < -m_jumpForce / 2)
                {
                    m_motion.y = -m_jumpForce / 2;
                }

                if (xInput == 0)
                {
                    m_motion.x = Mathf.Lerp(m_motion.x, 0, m_airResistance);
                }

                m_animatedSprite.Play(m_motion.y < 0 ? "JumpUp" : "JumpDown");
            }
            m_motion = MoveAndSlide(m_motion, Vector2.Up);
        }
    }
}