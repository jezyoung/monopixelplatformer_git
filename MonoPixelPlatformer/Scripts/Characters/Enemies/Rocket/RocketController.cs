using Godot;

namespace MonoPixelPlatformer.Scripts.Characters.Enemies.Rocket
{
    public class RocketController: RigidBody2D
    {
        private Sprite m_sprite;
        
        public override void _Ready()
        {
            m_sprite = GetNode<Sprite>("Sprite");
        }

        void _on_Rocket_body_entered(Node node)
        {
            GD.Print("hit!");
            QueueFree();
        }

        public void FlipDir(bool flip)
        {
            m_sprite.FlipH = flip;
        }
    }
}