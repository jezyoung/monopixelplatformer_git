using Godot;

namespace MonoPixelPlatformer.Scripts.Characters.Enemies.Rocket
{
    public class RocketLauncher: Node2D
    {
        [Export()] private float m_launchDelay = 5;
        [Export()] private float m_rocketXVelocity;
        [Export()] private float m_rocketXForce;
        [Export()] private bool m_flipDirection;
        
        private PackedScene m_rocketResource;
        private RigidBody2D m_rocket;
        private Timer m_launchTimer;

        public override void _Ready()
        {
            m_rocketResource = (PackedScene) ResourceLoader.Load("res://Scenes/Characters/Enemies/Rocket/Rocket.tscn");
            m_launchTimer = new Timer();
            AddChild(m_launchTimer);
            LaunchTimer();
        }

        async void LaunchTimer()
        {
            LaunchRocket();
            m_launchTimer.Start(m_launchDelay);
            await ToSignal(m_launchTimer, "timeout");
            LaunchTimer();
        }
        
        void LaunchRocket()
        {
            m_rocket = (RigidBody2D) m_rocketResource.Instance();
            m_rocket.LinearVelocity = new Vector2(m_rocketXVelocity, 0);
            m_rocket.AppliedForce = new Vector2(m_rocketXForce, 0);
            // RocketController rocketController = GetNode<RigidBody2D>(m_rocketResource.ResourcePath) as RocketController;
            // rocketController.FlipDir(m_flipDirection);
            AddChild(m_rocket);
            m_rocket.SetAsToplevel(true);
        }
    }
}